defmodule Egghat.Recipe do
  use Ecto.Schema
  import Ecto.Changeset

  schema "recipes" do
    field :name, :string
    field :image, :string

    belongs_to :user, Egghat.Accounts.User

    has_many :instructions, Egghat.Instruction
    has_many :ingredients, Egghat.Ingredient

    timestamps()
  end

  @doc false
  def changeset(recipe, attrs) do
    recipe
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end

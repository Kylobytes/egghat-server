defmodule Egghat.Repo do
  use Ecto.Repo,
    otp_app: :egghat,
    adapter: Ecto.Adapters.Postgres
end

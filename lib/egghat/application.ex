defmodule Egghat.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Egghat.Repo,
      # Start the Telemetry supervisor
      EgghatWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Egghat.PubSub},
      # Start the Endpoint (http/https)
      EgghatWeb.Endpoint
      # Start a worker by calling: Egghat.Worker.start_link(arg)
      # {Egghat.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Egghat.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    EgghatWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
